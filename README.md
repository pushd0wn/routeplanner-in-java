# A Web-Based Route Planner v1.0
By *Maher Albaba* and *Maximilian Kotowsky*.

A web-based route planner, implemented in the course of the programming project at the 
University of Stuttgart in 2019. The route planner implements a version of Dijkstra's
Shortest Path First algorithm, invented by Edsger W. Dijkstra in 1956, published in 1959.

## Installation
The installation works on *Ubuntu 18.04* and *Debian 9*.

Since it is not predictable which components are already installed on the target 
system, it is recommended to follow the instructions precisely. All specified commands
work on *Debian 9* and *Ubuntu 18.04*. For executing the following commands open a Terminal 
and enter them in the exact same order as specified.

### System Requirements
Recommended *(as specified or equivalent)*

    OS: Ubuntu 18.04
    Processor: i5 Haswell, 3.2 GHz
    Memory: 8GB RAM
    Java: OpenJDK v1.8 or Oracle Java v8

### Manual Installation
#### Java

If Java is not installed or not up-to-date take the following few steps.

*Update* the packet manager and *upgrade* all installed packages.

    sudo apt-get update && sudo apt-get -y upgrade    
Install *OpenJDK v1.8*.

    sudo apt-get -y install openjdk-8-jdk   
Check if OpenJDK version is 1.8

    java -version  
This should lead to the following or similar output.  

    openjdk version "1.8.0_212"
    OpenJDK Runtime Environment (build 1.8.0_212-8u212-b03-0ubuntu1.18.04.1-b03)
    OpenJDK 64-Bit Server VM (build 25.212-b03, mixed mode)  

#### Gradle
After Java is installed and works properly we need to install Gradle.

*Update* the packet manager and *upgrade* all installed packages.

    sudo apt-get update && sudo apt-get -y upgrade
Install *unzip* and *wget*.

    sudo apt-get install unzip wget
Download latest gradle version (v5.4.1).  
*NOTE: Downloaded zip-archive can be removed from disk after gradle was installed 
successfully.*

    wget https://services.gradle.org/distributions/gradle-5.4.1-all.zip  
Make directory for gradle and extract downloaded archive to the new directory.
    
    sudo mkdir /opt/gradle  
    sudo unzip gradle-5.4.1-all.zip -d /opt/gradle/  
Configure the PATH environment variable what is essential for directly executing 
the gradle executable anywhere on the system.

    export PATH=$PATH:/opt/gradle/gradle-5.4.1/bin
    
*NOTE: This information is mainly for user information. If current terminal
is closed the PATH environment variable will be resetted. Hence PATH will 
be set every time either ./routeplanner or ./benchmarker are executed.*

Check if gradle was installed successfully.
  
    gradle -v  
This should lead to the following or similar output.  

    ------------------------------------------------------------ 
    Gradle 5.4.1  
    ------------------------------------------------------------

    Build time:   2019-04-26 08:14:42 UTC
    Revision:     261d171646b36a6a28d5a19a69676cd098a4c19d

    Kotlin:       1.3.21
    Groovy:       2.5.4
    Ant:          Apache Ant(TM) version 1.9.13 compiled on July 10 2018
    JVM:          1.8.0_144 (Oracle Corporation 25.144-b01)
    OS:           Linux 4.9.0-8-amd64 amd64

### Automatic Installation

Since a manual installation as specified above is a lot of work to do and not all
users of our route planner are familiar with a terminal, we created an installer script.
The script accomplishes four main tasks:
* Installation of the latest versions of *OpenJDK 1.8*, *unzip* and *wget*
* Downloading *Gradle v5.4.1* from [Gradle Website](https://services.gradle.org/distributions/)
(zip-archive will be removed from disk after the installation of Gradle succeeded)
* Installation of Gradle in directory */opt/gradle/*
* Update PATH environment variable (is updated every time the user runs either *./benchmarker*
or *./routeplanner*)

*NOTE: The script needs to be executed with root privileges!*

For executing the installation script open a Terminal and type in the following command.

    sudo ./install
    
This may take a few minutes.

## File Format

For all modes it is important that the referred files are correct 
formatted as specified on [FMI](https://www.fmi.uni-stuttgart.de/alg/research/stuff/)
Homepage and the paths are relative or absolute.

     ---------------------------------------
    |    File            |   Format         |
    |---------------------------------------|
    |    map file        |   *.fmi          |
    |    query file      |   *.que          |
    |    solution file   |   *.sol          |
     ---------------------------------------
    
## CommandLine with different operating modes
In the following we will explain how to work with the route planner 
and what are its operating modes for.  
    
To start the routeplanner simply enter

    ./routeplanner
    
After Gradle has been initiated the user will be asked for a map file as specified in section 'File Format'. 
It may take a few seconds up to a minute to read in the map file, depending on its size. 
In the following a main menu is printed to the command line with four different modes.

*NOTE: It is always possible to return to main menu by entering 'menu' instead of a node!*

### (1) Benchmark
The 'Benchmark' mode is for automatically works off queries specified in a
query file and compares the results to sample solutions given by a solution file.
In details this means the route planner computes, based
on a certain map, queries, specified by a query file and compares the
results of the computation with solutions specified by a solution file.

### (2) One-To-All
The user is asked for a source node for which the distances to all other nodes on the graph
are computed. Afterwards the user can enter several destinations and query their distances to
the source node.  
If you want to enter a new source it is sufficient to type in 'menu' (return to main menu) and
choose *(2) One-To-All* again.

### (3) One-To-One
The user is asked for a source and a destination node for which the distance from source to 
destination should be computed.

### (4) Nearest Neighbour (Iterative)
The user is asked for coordinates (latitude and longitude, given as double precision values)
and the nearest neighbour contained in the graph is printed out.  
Search for the nearest neighbour is performed iterative, i.e. by iterating through the whole
graph.

*NOTE*: Double precision values are entered as a floating point number. 
For example as '10.01'.

### (5) Nearest Neighbour (By K-d tree)
The user is asked for coordinates (latitude and longitude, given as double precision values)
and the nearest neighbour contained in the graph is printed out.  
Search for the nearest neighbour is performed on a K-d tree.  
For more information see [Wikipedia](https://en.wikipedia.org/wiki/K-d_tree).

*NOTE*: Double precision values are entered as a floating point number. 
For example as '10.01'.

### (6) Average time of finding Nearest Neighbour in K-d tree
100 searches are performed on K-d tree with random coordinates. Afterwards the average time 
for a search is calculated and printed to the console.  
If average time is above 5 milliseconds a warning is printed out.

### (7) New Map File
The user can enter a new map file to operate on. This may take a few seconds up to a minute,
depending on the maps size.

### (8) Rebuild K-d tree
Due to performance the K-d tree is not built perfectly balanced. This mode rebuilds the K-d tree.
You can use this mode if average search in tree is too slow.

## Logs
All outputs during the execution are logged in directory *logs/*. The log files are 
named as follows

    LogFile_YYYY-MM-DD.seconds.log
    
    YYYY    = Year in which execution was performed
    MM      = Month in which execution was performed
    DD      = Day on which execution was performed
    seconds = Seconds since 1970-01-01 00:00:00 UTC

The seconds are added to the file name since we need to make a log file unique.

## JUnit Test
In phase 2 one exercise had been to implement a JUnit Test which.  
The test could be found in coordinates *src/test/java/NeighbourTest*.
The test chooses 100 random coordinates within the minimum bounding box of a given graph, 
defined by the variables *coordinates*. It searches for the nearest neighbour of all coordinates
as well by the iterative method as by the K-d tree method. All results are compared during search.

If the test fails for at least one iteration the test throws an AssertionError

To start the JUnit test execute the script 'junit_test' with coordinates to map file as parameter.

    ./junit_test /coordinates/to/map.fmi
    
## Server (Phase 3)
A webserver for computation and visualization of shortest path is provided. 

To start the server simply enter

    ./startServer
    
After Gradle has been initialized the user will be asked for a map file as specified in section 'File Format'. 
It may take a few seconds up to a minute to read in the map file, depending on its size. 


### Graphical User Interface

The GUI shows a map of germany, based on leaflet.js and provided by tile.openstreetmap.de
on which the user could select source and destination and the shortest path between those two points
is visualized on the map. The points can be entered either by clicking on the map or by entering coordinates on the right-handed interface.

For more information follow the link "Getting Started" at the bottom of the right-handed interface.