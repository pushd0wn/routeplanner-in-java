/* By Maximilian Kotowsky, 07.08.2019 */

var germany = L.map('map').setView([51.5, 10.0], 6);

var osm = L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {maxZoom: 18, attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                });
    osm.addTo(germany);

var myStyle = {
    "color" : "#322521",
    "weight" : 6,
    "opacity" : 0.65
};


//var start = L.popup({autoClose : false, closeOnClick : false});
var startMarker = {};
var startCoord = null;
var startID = -1;
//var end = L.popup({autoClose : false, closeOnClick : false});
var endMarker = {};
var endCoord = null;
var endID = -1;
var possibleMarker;
var possibleMarkerCoord = null;
var possibleMarkerID = -1;
var mode = true;
var path;
var layerGroup = new L.LayerGroup();
layerGroup.addTo(germany);

var avoMarker = L.icon({
    iconUrl: 'media/CustomMarker.png',

    iconSize:     [64, 64],
    iconAnchor:   [32, 52],
    tooltipAnchor:[32,-48]
});

function setMarker(e){
    var xhr = new XMLHttpRequest();
    xhr.open("get", `/nearest?lat=${e.latlng.lat}&lon=${e.latlng.lng}`);
    xhr.onreadystatechange = function() {
        if(xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.responseText);
            var res = JSON.parse(this.response);
            if(possibleMarker != undefined){
                germany.removeLayer(possibleMarker);
            }
            possibleMarker = L.marker([res.lat, res.lon], {icon: avoMarker});
            possibleMarker
                .addTo(germany)
            possibleMarkerCoord = [res.lat, res.lon];
            possibleMarkerID = res.nearestNodeID;
        }
    };
    xhr.send();
}

function getNextNode(e){
    var xhr = new XMLHttpRequest();
    xhr.open("get", `/nearest?lat=${e.latlng.lat}&lon=${e.latlng.lng}`);
    xhr.onreadystatechange = function() {
        if(xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.responseText);
            var res = JSON.parse(this.response);
            if (mode) {
                //var content = ` Source: <b>${res.nearestNodeID}</b><br/><br/>Lat: <b>${res.lat}</b><br/>Lon: <b>${res.lon}</b>`;
                // start
                    //.setLatLng(L.latLng(res.lat, res.lon))
                    //.setContent(content)
                    //.openOn(germany);
                if(startMarker != undefined){
                    germany.removeLayer(startMarker);
                }
                startMarker = L.marker([res.lat, res.lon], {icon: avoMarker});
                startMarker
                    .addTo(germany)
                    .bindTooltip("Start", 
                    {
                        permanent: true, 
                        direction: 'top'
                    }
                    )
                startCoord = [res.lat, res.lon];
                startID = res.nearestNodeID;
                document.getElementById("sourceNodeID").innerHTML = res.nearestNodeID;
                document.getElementById("latitudeSource").value = res.lat;
                document.getElementById("longitudeSource").value = res.lon;
            } else {
                //var content = ` Destination: <b>${res.nearestNodeID}</b><br/><br/>Lat: <b>${res.lat}</b><br/>Lon: <b>${res.lon}</b>`;
                //end
                    //.setLatLng(L.latLng(res.lat, res.lon))
                    //.setContent(content)
                    //.openOn(germany);
                if(endMarker != undefined){
                    germany.removeLayer(endMarker);
                }
                endMarker = L.marker([res.lat, res.lon], {icon: avoMarker});
                endMarker
                    .addTo(germany)
                    .bindTooltip("Destination", 
                    {
                        permanent: true, 
                        direction: 'top'
                    }
                    )
                endCoord = [res.lat, res.lon]
                endID = res.nearestNodeID;
                document.getElementById("destinationNodeID").innerHTML = res.nearestNodeID;
                document.getElementById("latitudeDest").value = res.lat;
                document.getElementById("longitudeDest").value = res.lon;
            }
            mode = !mode;
            if(path != null){
                layerGroup.removeLayer(path);
                document.getElementById("distance").innerHTML = "Not Available";
            }
          }
    };
    xhr.send();
}

function setMarkerAsSource(){
    if(possibleMarker != undefined){
        if(startMarker != undefined){
            germany.removeLayer(startMarker);
        }
        if(path != null){
            layerGroup.removeLayer(path);
            document.getElementById("distance").innerHTML = "Not Available";
        }
        germany.removeLayer(possibleMarker);
        startMarker = L.marker([possibleMarkerCoord[0], possibleMarkerCoord[1]], {icon: avoMarker});
        startMarker
            .addTo(germany)
            .bindTooltip(` Source: <b>${possibleMarkerID}</b><br/>Lat: <b>${possibleMarkerCoord[0]}</b><br/>Lon: <b>${possibleMarkerCoord[1]}</b>`, 
            {
                permanent: true, 
                direction: 'top'
            })
        startCoord = [possibleMarkerCoord[0], possibleMarkerCoord[1]];
        startID = possibleMarkerID;
        possibleMarkerCoord = null;
        possibleMarkerID = -1;
        document.getElementById("sourceNodeID").innerHTML = startID;
        document.getElementById("latitudeSource").value = startCoord[0];
        document.getElementById("longitudeSource").value = startCoord[1];
    } else {
        alert("No Marker is set!")
    }
}

function setMarkerAsDestination(){
    if(possibleMarker != undefined){
        if(endMarker != undefined){
            germany.removeLayer(endMarker);
        }
        if(path != null){
            layerGroup.removeLayer(path);
            document.getElementById("distance").innerHTML = "Not Available";
        }
        germany.removeLayer(possibleMarker);
        endMarker = L.marker([possibleMarkerCoord[0], possibleMarkerCoord[1]], {icon: avoMarker});
        endMarker
            .addTo(germany)
            .bindTooltip(` Destination: <b>${possibleMarkerID}</b><br/>Lat: <b>${possibleMarkerCoord[0]}</b><br/>Lon: <b>${possibleMarkerCoord[1]}</b>`, 
            {
                permanent: true, 
                direction: 'top'
            })
        endCoord = [possibleMarkerCoord[0], possibleMarkerCoord[1]];
        endID = possibleMarkerID;
        possibleMarkerCoord = null;
        possibleMarkerID = -1;
        document.getElementById("destinationNodeID").innerHTML = endID;
        document.getElementById("latitudeDest").value = endCoord[0];
        document.getElementById("longitudeDest").value = endCoord[1];

    } else {
        alert("No Marker is set!")
    }
}

function setSource(data){
    var xhr = new XMLHttpRequest();
    xhr.open("get", `/nearest?lat=${data.latitudeSource.value}&lon=${data.longitudeSource.value}`);
    xhr.onreadystatechange = function() {
        if(xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.responseText);
            var res = JSON.parse(this.response);
            //var content = ` Source: <b>${res.nearestNodeID}</b><br/><br/>Lat: <b>${res.lat}</b><br/>Lon: <b>${res.lon}</b>`;
            //start
                //.setLatLng(L.latLng(res.lat, res.lon))
                //.setContent(content)
                //.openOn(germany);
            if(startMarker != undefined){
                germany.removeLayer(startMarker);
            }
            if(path != null){
                layerGroup.removeLayer(path);
                document.getElementById("distance").innerHTML = "Not Available";
            }
            startMarker = L.marker([res.lat, res.lon], {icon: avoMarker});
            startMarker
                    .addTo(germany)
                    .bindTooltip(` Source: <b>${res.nearestNodeID}</b><br/>Lat: <b>${res.lat}</b><br/>Lon: <b>${res.lon}</b>`, 
                    {
                        permanent: true, 
                        direction: 'top'
                    }
                    )
            startCoord = [res.lat, res.lon];
            startID = res.nearestNodeID;
            document.getElementById("sourceNodeID").innerHTML = res.nearestNodeID;
            document.getElementById("latitudeSource").value = res.lat;
            document.getElementById("longitudeSource").value = res.lon;
          }
    };
    xhr.send();
}

function setDestination(data){
    var xhr = new XMLHttpRequest();
    xhr.open("get", `/nearest?lat=${data.latitudeDest.value}&lon=${data.longitudeDest.value}`);
    xhr.onreadystatechange = function() {
        if(xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.responseText);
            var res = JSON.parse(this.response);
            //var content = ` Destination: <b>${res.nearestNodeID}</b><br/><br/>Lat: <b>${res.lat}</b><br/>Lon: <b>${res.lon}</b>`;
            //end
                //.setLatLng(L.latLng(res.lat, res.lon))
                //.setContent(content)
                //.openOn(germany);
            if(endMarker != undefined){
                germany.removeLayer(endMarker);
            }
            if(path != null){
                layerGroup.removeLayer(path);
                document.getElementById("distance").innerHTML = "Not Available";
            }
            endMarker = L.marker([res.lat, res.lon], {icon: avoMarker});
                endMarker
                    .addTo(germany)
                    .bindTooltip(` Destination: <b>${res.nearestNodeID}</b><br/>Lat: <b>${res.lat}</b><br/>Lon: <b>${res.lon}</b>`, 
                    {
                        permanent: true, 
                        direction: 'top'
                    }
                    )   
            endCoord = [res.lat, res.lon];
            endID = res.nearestNodeID;
            document.getElementById("destinationNodeID").innerHTML = res.nearestNodeID;
            document.getElementById("latitudeDest").value = res.lat;
            document.getElementById("longitudeDest").value = res.lon;
          }
    };
    xhr.send();
}

//germany.on('click', getNextNode);
germany.on('click', setMarker);

function sendRequest(){

    if(startCoord != null && endCoord != null){
        var xhr = new XMLHttpRequest();

        xhr.open("get", `http://localhost:8080/path?lat1=${startCoord[0]}&lon1=${startCoord[1]}&lat2=${endCoord[0]}&lon2=${endCoord[1]}`);
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && xhr.status == 200) {
                var res = JSON.parse(this.response);
                if(res.distance != -1){
                    console.log(res);
                    if (path != null) {
                        layerGroup.removeLayer(path);
                        document.getElementById("distance").innerHTML = "Not Available";
                    }
                    path = new L.geoJSON({coordinates:res.coordinates, type:res.type}, {style:myStyle});
                    layerGroup.addLayer(path);
                    document.getElementById("distance").innerHTML = res.distance;
                } else {
                    alert(`No Route available from Source ${startID} to Destination ${endID}!`);
                }
            }
        };
        xhr.send();
    } else {
        alert("Source or Destination not defined!");
    }
}

function clearMap(){

    if(path != null){
        layerGroup.removeLayer(path);
    }
    if(startMarker != undefined){
        germany.removeLayer(startMarker);
    }
    if(endMarker != undefined){
        germany.removeLayer(endMarker);
    }
    if(possibleMarker != undefined){
        germany.removeLayer(possibleMarker);
    }

    startCoord = null;
    startID = -1;
    endCoord = null;
    endID = -1;
    possibleMarkerCoord = null;
    possibleMarkerID = -1;

    germany.setView([51.5, 10.0], 6);

    document.getElementById("distance").innerHTML = "Not Available";
    document.getElementById("sourceNodeID").innerHTML = "Not Available";
    document.getElementById("destinationNodeID").innerHTML = "Not Available";
    document.getElementById("latitudeSource").value = '';
    document.getElementById("longitudeSource").value = '';
    document.getElementById("latitudeDest").value = '';
    document.getElementById("longitudeDest").value = '';
}

function popup(mylink, windowname) { 
    if (! window.focus)return true;
    var href;
    var w = window.innerWidth - 100;
    var h = window.innerHeight - 200;
    var y = window.outerHeight / 2 + window.screenY - ( h / 2)
    var x = window.outerWidth / 2 + window.screenX - ( w / 2)
    if (typeof(mylink) == 'string') href=mylink;
    else href=mylink.href; 
    
    window.open(href, windowname, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);
    return false; 
}

/* For source see https://harlemsquirrel.github.io/css/javascript/2017/01/31/dark-light-mode-switcher.html */

function toggleDarkLight() {
    var body = document.getElementById("body");
    var currentClass = body.className;
    if (currentClass == "dark-mode"){
        document.getElementById("logo").src = "media/Logo/2LightFavicon1000.png"
        document.getElementById("mode").value = "Dark Mode"
    } else {
        document.getElementById("logo").src = "media/Logo/2DarkFavicon1000.png"
        document.getElementById("mode").value = "Light Mode"
    }
    body.className = currentClass == "dark-mode" ? "light-mode" : "dark-mode";
}
  