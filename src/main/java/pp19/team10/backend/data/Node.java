package pp19.team10.backend.data;

/**
 * Node to store data for a point in KDTree. Each node consists of a ID (references to the node on the graph)
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class Node {

    // variables
    private int id; // ID - reference to the corresponding node on the graph
    private boolean isLeaf; // true if a node is a leaf
    private Node leftChild; // left child in KDTree
    private Node rightChild; // right child in KDTree

    /**
     * Constructor for Node
     *
     * @param graphNode Number of the corresponding node in the graph
     */
    public Node(int graphNode) {
        this.id = graphNode;
        this.isLeaf = true;
    }

    /**
     * Getter for class attribute id
     *
     * @return id of node
     */
    public int getId() {
        return id;
    }

    /**
     * Getter for reference to left child of the node in KDTree
     *
     * @return left child of the node
     */
    public Node getLeftChild() {
        return leftChild;
    }

    /**
     * Setter for reference to left child of the node in KDTree
     *
     * @param leftChild left child of the node
     */
    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
        this.isLeaf = false;
    }

    /**
     * Getter for reference to right child of the node in KDTree
     *
     * @return right child of the node
     */
    public Node getRightChild() {
        return rightChild;
    }

    /**
     * Setter for reference to right child of the node in KDTree
     *
     * @param rightChild right child of the node
     */
    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
        this.isLeaf = false;
    }

    /**
     * Checks if the node is a leaf node
     *
     * @return true if the node is a leaf node, otherwise false
     */
    public boolean isLeaf() {
        return this.isLeaf;
    }
}
