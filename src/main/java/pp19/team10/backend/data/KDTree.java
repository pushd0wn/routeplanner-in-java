package pp19.team10.backend.data;

import java.util.Arrays;

/**
 * Class KDTree for efficient search of nearest neighbour in graph
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class KDTree {

    // variables

    private int numberOfNodes; // store number of nodes in graph
    private Node root; // root of KDTree

    private double[] longitudes; // store longitude array of given graph
    private double[] latitudes; // store latitude array of given graph

    /**
     * Constructor for static KDTree, i.e. the tree can't be changed after it has been
     * built once. Tree is automatically built by constructor.
     *
     * @param graph Graph over which the KDTree should be built
     */
    public KDTree(Graph graph) {
        this.numberOfNodes = graph.getNumberOfNodes();

        this.latitudes = graph.getLatitudes();
        this.longitudes = graph.getLongitudes();

        // build KDTree
        build();
    }

    /**
     * Method for building a static KDTree
     */
    private void build() {

        // compute median out of numberOfRandom random values for root node. This idea had been adapted from wikipedia
        int indexMedian = median(this.longitudes);

        // insert 'median' as root
        this.root = insertNode(indexMedian, null, true);

        // iterate through all nodes and insert them into the KDTree
        for (int i = 0; i < this.numberOfNodes; i++) {
            // since 'median' had already been inserted skip 'median'node
            if (i != indexMedian) root = insertNode(i, this.root, true);
        }
    }

    /**
     * Insert node by ID recursively into the KDTree
     *
     * @param id      ID of the node (on the graph) which should be inserted into the KDTree
     * @param current Current root of the subtree in which the ID should be inserted
     * @param mode    Cutting dimension, i.e. in which dimension the split should be done.
     *                True = Longitude, False = Latitude
     * @return A new node (referring to the node on the graph with ID) if a Leaf node
     * had been reached or the node (for recursion)
     */
    private Node insertNode(int id, Node current, boolean mode) {
        // insert new node by ID if parent is a leaf or subtree is empty
        if (current == null) return new Node(id);

        /* check if (id) node's values are less, equal or greater than those of current node.
         * Depending on cutting dimension latitudes or longitudes are compared.
         * Mode = true -> Longitude
         * Mode = false -> Latitude
         */

        // if value of new node (id) is less than that of current node store new node in left subtree
        if (compare(id, current.getId(), mode)) {
            current.setLeftChild(insertNode(id, current.getLeftChild(), !mode));
        }
        // if value of new node (id) is greater or equal than that of current node store new node in right subtree
        else {
            current.setRightChild(insertNode(id, current.getRightChild(), !mode));
        }

        // break out of recursion
        return current;
    }

    /**
     * Compare two nodes by latitude or longitude value.
     * Depends on mode which values are compared.
     *
     * @param t1   ID of first node
     * @param t2   ID of second node
     * @param mode If mode == true -> compareLEQ longitudes
     *             If mode == false -> compareLEQ latitudes
     * @return true if value of first ID is less than that of second ID
     */
    private boolean compare(int t1, int t2, boolean mode) {
        if (mode) return (this.longitudes[t1] < this.longitudes[t2]);
        return (this.latitudes[t1] < this.latitudes[t2]);
    }

    /**
     * This Method chooses the median out of the given array
     *
     * @param array Array of values out of which median should be computed
     * @return Median of the arrays entries
     */
    private int median(double[] array) {
        // variables
        double[] temp = new double[array.length];

        System.arraycopy(array, 0, temp, 0, array.length);

        // sort array
        Arrays.sort(temp);

        // store median
        double median = temp[temp.length / 2];

        // return index of median value
        for (int i = 0; i < temp.length; i++) {
            if (array[i] == median) return i;
        }


        // return -1 if n == 0
        return -1;
    }

    /**
     * Getter for longitude array
     *
     * @return longitude array of corresponding graph
     */
    public double[] getLongitudes() {
        return longitudes;
    }

    /**
     * Getter for latitude array
     *
     * @return latitude array of corresponding graph
     */
    public double[] getLatitudes() {
        return latitudes;
    }

    /**
     * Getter for ID (in graph) of root node.
     *
     * @return ID of root node
     */
    public Node getRoot() {
        return this.root;
    }
}
