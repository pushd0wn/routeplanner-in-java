package pp19.team10.backend.data;

/**
 * Class for a directed weighted graph
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class Graph {

    private int numberOfNodes; // # Nodes/Vertices in Graph
    private int numberOfEdges; // # Edges in Graph

    // node arrays

    /* Edge-offsets for nodes, i.e. edgeOffset[i] = index at which the
     * sub arrays of adjacent (outgoing) edges for node i start in edge arrays
     */
    private int[] edgeOffset;
    private double[] longitudes; // longitudes of nodes
    private double[] latitudes; // latitudes of nodes

    // edge arrays

    private int[] destination; // heads of directed edges
    private int[] weight; // weights of directed edges

    private double[] minBoundingBox;

    /**
     * Constructor for class Graph
     *
     * @param numberOfNodes <b>int</b> number of nodes / vertices in graph
     * @param numberOfEdges <b>int</b> number of edges in graph (directed, weighted)
     * @param offsets       <b>int[]</b> array of offsets, i.e. where in arrays <i>destination</i>
     *                      and <i>weight</i> the sub array of edges adjacent to the node with name index starts
     * @param destinations  <b>int[]</b> array of destinations / heads of edges in graph (edges are directed)
     * @param weights       <b>int[]</b> array of weights of edges (edges are weighted)
     * @param longitudes    <b>double[]</b> array of longitudes of nodes
     * @param latitudes     <b>double[]</b> array of latitudes of nodes
     */
    public Graph(int numberOfNodes, int numberOfEdges, int[] offsets, int[] destinations, int[] weights,
                 double[] longitudes, double[] latitudes) {
        this.numberOfEdges = numberOfEdges;
        this.numberOfNodes = numberOfNodes;
        this.edgeOffset = offsets;
        this.destination = destinations;
        this.weight = weights;
        this.longitudes = longitudes;
        this.latitudes = latitudes;
        this.minBoundingBox = calcMinBoundingBox();
    }


    /**
     * Getter for number of nodes in graph
     *
     * @return <b>int</b> number of nodes in graph
     */
    public int getNumberOfNodes() {
        return numberOfNodes;
    }

    /**
     * Getter for number of edges in graph. Edges are directed and
     * weighted (see parameter edgeOffset, destination, weights)
     *
     * @return <b>int</b> number of edges in graph
     */
    public int getNumberOfEdges() {
        return numberOfEdges;
    }

    /**
     * Returns offset of a given node, i.e. where in arrays <i>destination</i> and <i>weight</i> the subarray of edges
     * adjacent to the given node starts
     *
     * @param node <b>int</b> node for which offset should be returned
     * @return <b>int</b> offset of given node
     */
    public int offsetOf(int node) {
        return edgeOffset[node];
    }

    /**
     * Returns destination / head of a given edge
     *
     * @param edge <b>int</b> edge of which head / destination should be returned
     * @return <b>int</b> destination / head of the given edge
     */
    public int destinationOf(int edge) {
        return destination[edge];
    }

    /**
     * Returns weight of a given edge
     *
     * @param edge <b>int</b> edge of which weight should be returned
     * @return <b>int</b> weight of the given edge
     */
    public int weightOf(int edge) {
        return weight[edge];
    }

    public double[] getLongitudes() {
        return longitudes;
    }

    public double[] getLatitudes() {
        return latitudes;
    }

    /**
     * Method for computing the minimum bounding box, i.e. a rectangle which contains the whole graph.
     * <p>
     * boundingBox[0] = maximum latitude which lies inside the bounding box
     * boundingBox[1] = maximum longitude which lies inside the bounding box
     * boundingBox[2] = minimum latitude which lies inside the bounding box
     * boundingBox[3] = minimum longitude which lies inside the bounding box
     *
     * @return A 2-dimensional double array containing the values of the outlines of the box
     */
    public double[] calcMinBoundingBox() {

        // init variables
        double minLon = Double.MAX_VALUE;
        double maxLon = Double.MIN_VALUE;
        double minLat = Double.MAX_VALUE;
        double maxLat = Double.MIN_VALUE;

        // search for minimum / maximum values of longitudes / latitudes
        for (int i = 0; i < this.getNumberOfNodes(); i++) {
            if (this.longitudes[i] < minLon) minLon = this.longitudes[i];
            if (this.longitudes[i] > maxLon) maxLon = this.longitudes[i];
            if (this.latitudes[i] < minLat) minLat = this.latitudes[i];
            if (this.latitudes[i] > maxLat) maxLat = this.latitudes[i];
        }

        // init bounding box and set values
        double[] boundingBox = new double[4];
        boundingBox[0] = maxLat;
        boundingBox[1] = maxLon;
        boundingBox[2] = minLat;
        boundingBox[3] = minLon;

        return boundingBox; // save boundingBox
    }

    /**
     * Getter for class variable minBoundingBox
     *
     * @return minimal bounding box of the graph, i.e. a rectangle which contains the graph completely
     */
    public double[] getMinBoundingBox() {
        return minBoundingBox;
    }
}


