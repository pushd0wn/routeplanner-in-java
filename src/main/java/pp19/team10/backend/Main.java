package pp19.team10.backend;

import pp19.team10.backend.cli.CommandLineInterface;
import pp19.team10.backend.server.Server;

/**
 * Main class to start via script
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        if (args[0].equals("cli")) {
            CommandLineInterface.mainCLI();
        } else if (args[0].equals("server")) {
            Server.mainServer(args);
        }
    }
}
