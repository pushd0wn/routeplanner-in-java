package pp19.team10.backend.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pp19.team10.backend.algorithms.Dijkstra;
import pp19.team10.backend.algorithms.Reader;
import pp19.team10.backend.data.Graph;
import pp19.team10.backend.data.KDTree;
import pp19.team10.backend.exceptions.IllegalFileFormatException;

import java.util.Scanner;

/**
 * Class for Java Spring Server
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
@SpringBootApplication
public class Server {

    // class attributes
    private static String path; // path to map file
    private static Dijkstra dijkstra; // dijkstra to compute shortest path and distance
    private static Graph graph; // graph on which dijkstra operates
    private static KDTree kdTree; // kdTree for nearest neighbour search

    /**
     * Method to run server
     *
     * @param args Arguments for Java Spring
     */
    public static void mainServer(String[] args) {
        printHeader(); // ask for map file
        try {
            // init variables
            graph = new Reader().readFile(path);
            dijkstra = new Dijkstra(graph);
            kdTree = new KDTree(graph);
        } catch (IllegalFileFormatException e) {
            e.printStackTrace();
        }
        // run server
        SpringApplication.run(Server.class, args);
    }

    /**
     * Method to print out Headline of Server
     */
    private static void printHeader() {
        Scanner sc = new Scanner(System.in);
        // Print out header
        System.out.println();
        System.out.println("Server for 'Programmierprojekt' 2019");
        System.out.println("Maher Albaba (3338083), Maximilian Kotowsky (3311068)");
        System.out.println();
        System.out.print("Please enter path to a map file: ");
        path = sc.next();
        System.out.println();
        System.out.println("Operating on map '" + cropPath(path) + "'");
        sc.close();
    }

    /**
     * Method to extract the name of a map off a given path
     *
     * @param path Path of map file
     * @return Name of the map
     */
    private static String cropPath(String path) {
        return path.substring(path.lastIndexOf('/') + 1, path.lastIndexOf('.'));
    }

    /**
     * Getter for class attribute dijkstra
     *
     * @return dijkstra of server
     */
    public static Dijkstra getDijkstra() {
        return dijkstra;
    }

    /**
     * Getter for class attribute graph
     *
     * @return graph of server
     */
    public static Graph getGraph() {
        return graph;
    }

    /**
     * Getter for class attribute kdTree
     *
     * @return kdTree of server
     */
    public static KDTree getKdTree() {
        return kdTree;
    }
}
