package pp19.team10.backend.server;

import java.util.List;

/**
 * Class for transfer of data between backend and frontend
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class DataPacket {

    // make code more readable
    private final int DISTANCE_INFINITY = -1;

    // class attributes
    private List<double[]> coordinates; // path between two points
    private int distance; // distance between two points
    private String type = "LineString"; // for geoJSON path
    private int nearestNodeID; // next node to a given coordinate
    private double lat; // latitude of next point
    private double lon; // longitude of next point
    private boolean errorOccured; // if error occured

    /**
     * Constructor for dataPacket, standard value for parameter is '-1' or 'false'
     */
    public DataPacket() {
        this.nearestNodeID = -1;
        this.lat = -1;
        this.lon = -1;
        this.errorOccured = false;
        this.distance = DISTANCE_INFINITY;
    }

    /**
     * Getter for class attribute nearestNodeID
     *
     * @return id of nearest node to a given coordinate
     */
    public int getNearestNodeID() {
        return nearestNodeID;
    }

    /**
     * Setter for class attribute nearestNodeID
     *
     * @param nearestNodeID id of nearest node to a given coordinate
     */
    public void setNearestNodeID(int nearestNodeID) {
        this.nearestNodeID = nearestNodeID;
    }

    /**
     * Getter for class attribute lat
     *
     * @return latitude of nearest node to a given coordinate
     */
    public double getLat() {
        return lat;
    }

    /**
     * Setter for class attribute lat
     *
     * @param lat latitude of nearest node to a given coordinate
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * Getter for class attribute lon
     *
     * @return longitude of nearest node to a given coordinate
     */
    public double getLon() {
        return lon;
    }

    /**
     * Setter for class attribute lon
     *
     * @param lon longitude of nearest node to a given coordinate
     */
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * Getter for class attribute errorOccured
     *
     * @return true if an error occured during computation of dijkstra else otherwise
     */
    public boolean isErrorOccured() {
        return errorOccured;
    }

    /**
     * Setter for class attribute errorOccured
     *
     * @param errorOccured error occured
     */
    public void setErrorOccured(boolean errorOccured) {
        this.errorOccured = errorOccured;
    }

    /**
     * Getter for class attribute coordinates
     *
     * @return List &lt;double[]&gt; which represents a path between two points. Contains coordinates as double[] of all
     * points on the path
     */
    public List<double[]> getCoordinates() {
        return coordinates;
    }

    /**
     * Setter for class attribute coordinates
     *
     * @param coordinates List &lt;double[]&gt; which represents a path between two points. Contains coordinates
     *                    as double[] of all points on the path
     */
    public void setCoordinates(List<double[]> coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Getter for class attribute distance
     *
     * @return distance between two points
     */
    public int getDistance() {
        return distance;
    }

    /**
     * Setter for class attribute distance
     *
     * @param distance distance between two points
     */
    public void setDistance(int distance) {
        this.distance = distance;
    }

    /**
     * Getter for class attribute type
     *
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * Setter for class attribute type
     *
     * @param type type
     */
    public void setType(String type) {
        this.type = type;
    }
}
