package pp19.team10.backend.server;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pp19.team10.backend.algorithms.SearchKDTree;
import pp19.team10.backend.exceptions.GraphException;

/**
 * Class for Controller to handle communication between frontend and backend
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
@RestController
public class Controller {

    // store KDTree for nearest neighbour search
    private SearchKDTree searchKDTree = new SearchKDTree(Server.getKdTree());

    /**
     * Compute path between two points.
     * Searches first the nearest points to the coordinates and computes a path afterwards
     *
     * @param lat1 Latitude of Source
     * @param lon1 Longitude of Source
     * @param lat2 Latitude of Destination
     * @param lon2 Longitude of Destination
     * @return DataPacket containing relevant information: distance (between the two points on graph),
     * errorOccured, coordinates (path between source and destination by coordinates of all points on path)
     */
    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/path")
    public DataPacket searchPath(@RequestParam double lat1, @RequestParam double lon1,
                                 @RequestParam double lat2, @RequestParam double lon2) {
        // find nearest nodes
        int startNode = searchKDTree.findNearestNeighbour(lat1, lon1);
        int destinationNode = searchKDTree.findNearestNeighbour(lat2, lon2);

        // init dataPacket to return values to frontend
        DataPacket dataPacket = new DataPacket();
        try {
            // compute and set distance in dataPacket
            dataPacket.setDistance(Server.getDijkstra().algorithm(startNode, destinationNode));
        } catch (GraphException e) {
            // set errorOccured since error happened
            dataPacket.setErrorOccured(true);
            return dataPacket;
        }
        // set coordinates to refer to path between source and destination
        dataPacket.setCoordinates(Server.getDijkstra().getLastPath());

        return dataPacket;
    }

    /**
     * Returns next point on graph to given coordinates
     *
     * @param lat Latitude of coordinate pair
     * @param lon Longitude of coordinate pair
     * @return DataPacket containing relevant information: nearestNodeID (next node to given coordinates),
     * lat, lon (coordinates of point)
     */
    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/nearest")
    public DataPacket searchNext(@RequestParam double lat, @RequestParam double lon) {
        // id of nearest node to given coordinates
        int id = searchKDTree.findNearestNeighbour(lat, lon);

        // init dataPacket to return values to frontend
        DataPacket dataPacket = new DataPacket();

        // set data of nearest point on graph
        dataPacket.setNearestNodeID(id);
        dataPacket.setLat(Server.getGraph().getLatitudes()[id]);
        dataPacket.setLon(Server.getGraph().getLongitudes()[id]);

        return dataPacket;
    }
}