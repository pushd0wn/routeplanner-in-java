package pp19.team10.backend.exceptions;

public class GraphException extends Exception {

    /**
     * Constructs an GraphException with no detail message.
     */
    public GraphException() {
        super();
    }

    /**
     * Constructs a new GraphException with the specified detail message.
     *
     * @param errorMessage the detail message.
     *                     The detail message is saved for later retrieval by the Throwable.getMessage() method.
     */
    public GraphException(String errorMessage) {
        super(errorMessage);
    }
}
