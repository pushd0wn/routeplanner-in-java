package pp19.team10.backend.exceptions;

public class IllegalNodeException extends GraphException {

    /**
     * Constructs an IllegalNodeException with no detail message.
     */
    public IllegalNodeException() {
        super();
    }

    /**
     * Constructs a new IllegalNodeException with the specified detail message.
     *
     * @param errorMessage the detail message.
     *                     The detail message is saved for later retrieval by the Throwable.getMessage() method.
     */
    public IllegalNodeException(String errorMessage) {
        super(errorMessage);
    }
}
