package pp19.team10.backend.exceptions;

public class NoPathAvailableException extends GraphException {

    /**
     * Constructs an NoPathAvailableException with no detail message.
     */
    public NoPathAvailableException() {
        super();
    }

    /**
     * Constructs a new NoPathAvailableException with the specified detail message.
     *
     * @param errorMessage the detail message.
     *                     The detail message is saved for later retrieval by the Throwable.getMessage() method.
     */
    public NoPathAvailableException(String errorMessage) {
        super(errorMessage);
    }
}
