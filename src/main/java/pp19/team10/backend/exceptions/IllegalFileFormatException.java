package pp19.team10.backend.exceptions;

public class IllegalFileFormatException extends Exception {

    /**
     * Constructs an IllegalFileFormatException with no detail message.
     */
    public IllegalFileFormatException() {
        super();
    }

    /**
     * Constructs a new IllegalFileFormatException with the specified detail message.
     *
     * @param errorMessage the detail message.
     *                     The detail message is saved for later retrieval by the Throwable.getMessage() method.
     */
    public IllegalFileFormatException(String errorMessage) {
        super(errorMessage);
    }
}
