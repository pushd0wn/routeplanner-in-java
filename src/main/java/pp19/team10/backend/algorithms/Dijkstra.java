package pp19.team10.backend.algorithms;

import pp19.team10.backend.data.Graph;
import pp19.team10.backend.exceptions.GraphException;
import pp19.team10.backend.exceptions.IllegalNodeException;
import pp19.team10.backend.exceptions.NoPathAvailableException;

import java.util.*;

/**
 * Class for dijkstra's algorithm for computation of a shortest path from a given source node to a certain
 * destination node on a directed, (non negative) weighted graph
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class Dijkstra {

    // variables for certain purposes whereas -1 is in general not used
    private static final int NO_PREVIOUS = -1;
    private static final int DISTANCE_INFINITY = -1;
    private static final int NOT_AVAILABLE = -1;
    private static final int ONE_TO_ALL_COMPLETE = -2;

    private Graph graph; // reference to a graph on which dijkstra's algorithm is executed
    private int[] distance; // distances of all nodes to source node during computation of dijkstra's algorithm
    private int[] previous; // previous node of all nodes regarding the current computation of dijkstra's algorithm
    /* false until node at index is selected during computation as currently
     * closest node to source */
    private boolean[] processed;
    private int previousSource; // source of preceding computation of dijkstra's algorithm
    private List<double[]> lastPath; // List containing all nodes of the path from last computation

    /**
     * Constructor for class Dijkstra
     *
     * @param graph <b>backend.Graph</b> Graph on which dijkstra's algorithm should be performed
     */
    public Dijkstra(Graph graph) {

        this.graph = graph;
        this.distance = new int[this.graph.getNumberOfNodes()];
        this.previous = new int[this.graph.getNumberOfNodes()];
        this.processed = new boolean[this.graph.getNumberOfNodes()];
        this.previousSource = NOT_AVAILABLE; // no previous source available if class dijkstra is new
        this.lastPath = new ArrayList<>();

        // initialize vectors with -1 (normally not in use) and false
        for (int i = 0; i < this.graph.getNumberOfNodes(); i++) {
            this.distance[i] = DISTANCE_INFINITY;
            this.previous[i] = NO_PREVIOUS;
            this.processed[i] = false;
        }
    }

    /*
     * Instantiate a priority queue with a modified comparator.
     * Nodes are compared concerning their current distance to the source
     *
     * Returns an instance of a priority queue
     */
    private Queue<Integer> initQueue(Dijkstra dijkstra) {
        Comparator<Integer> comparator;
        comparator = (t1, t2) -> {
            if (dijkstra.distance[t2] == DISTANCE_INFINITY || dijkstra.distance[t1] < dijkstra.distance[t2]) {
                return -1;
            } else if (dijkstra.distance[t1] == DISTANCE_INFINITY || dijkstra.distance[t1] > dijkstra.distance[t2]) {
                return 1;
            }
            return 0;
        };
        return new PriorityQueue<>(comparator);
    }

    /**
     * Executes Dijkstra's algorithm from a source to a destination node OR a one-to-all calculation
     * from source.
     * <p>
     * Execute a calculation of Dijkstra's algorithm with a given node as source to a destination
     * node or, if no destination node is entered, a one-to-all calculation from source.<br/>
     * After the algorithm has terminated the distance to the destination node will be returned or, if no
     * destination had been specified, a distance to a certain destination node could be queried.
     *
     * <b>Precondition:</b> IDs of source and the destination node has to be positive integers
     *
     * @param source ID of the source node for the shortest path calculation
     * @return Returns the shortest distance from source to destination node or -1 if no destination has
     * been specified and the algorithm terminates.
     */
    public int oneToAll(int source) throws GraphException {
        return algorithm(source, NOT_AVAILABLE);
    }

    /**
     * Executes Dijkstra's algorithm from a source to a destination node OR a one-to-all calculation
     * from source.
     * <p>
     * Execute a calculation of Dijkstra's algorithm with a given node as source to a destination
     * node or, if no destination node is entered, a one-to-all calculation from source.<br/>
     * After the algorithm has terminated the distance to the destination node will be returned or, if no
     * destination had been specified, a distance to a certain destination node could be queried.
     *
     * <b>Precondition:</b> IDs of source and the destination node has to be positive integers
     *
     * @param source      ID of the source node for the shortest path calculation
     * @param destination ID of the destination node for the shortest path calculation
     * @return Returns the shortest distance from source to destination node or -1 if no destination has
     * been specified and the algorithm terminates.
     * @throws GraphException throws IllegalNodeException if specified destination is less than -1
     *                        (-1 implies a one to all computation of dijkstra)
     */

    public int algorithm(int source, int destination) throws GraphException {

        /*
         * If specified destination is less than -1 (i.e. NO_DESTINATION -> one to all computation) or
         * source is less than 0 throw new IllegalNodeException since they're not part of the underlying
         * graph
         */
        if ((destination < -1) || (source < 0))
            throw new IllegalNodeException("Please enter only node indices with value greater or equal 0.");

        /*
         * If specified source or destination are not part of the underlying graph throw new IllegalNodeException
         */
        if ((source > this.graph.getNumberOfNodes() - 1) || (destination > this.graph.getNumberOfNodes() - 1))
            throw new IllegalNodeException("Please enter only nodes which are part of the given graph.");
        /*
         * If a destination had been specified and the calculation had already be done for
         * the same source (must have been one to all, not aborted) simply return the calculated
         * distance: source -> destination
         */
        if (previousSource == source && destination != NOT_AVAILABLE) {
            computeLastPath(source, destination);
            return distance[destination];
        }
        // Priority Queue for the 'rim' of nodes
        Queue<Integer> queue = initQueue(this);

        /*
         * Re-initialize vectors if source node is a new one,
         * i.e. calculation has to be done once again
         */
        for (int i = 0; i < graph.getNumberOfNodes(); i++) {
            this.distance[i] = DISTANCE_INFINITY; // Set all distances to infinity
            this.previous[i] = NO_PREVIOUS; // Reset all previous nodes
            this.processed[i] = false; // Reset processed array
        }

        // initialize values for source node
        this.distance[source] = 0; // distance from source to source is zero
        this.previous[source] = NO_PREVIOUS; // source has no previous node
        queue.add(source); // Add source to the 'rim'

        /*

        Check Java Heap Size for Debugging (left in code for later use)

        // Get current size of heap in bytes
        long heapSize = Runtime.getRuntime().totalMemory();

        // Get maximum size of heap in bytes. The heap cannot grow beyond this size.// Any attempt will result in an OutOfMemoryException.
        long heapMaxSize = Runtime.getRuntime().maxMemory();

        System.out.println("heapSize: " + heapSize);
        System.out.println("MaxHeapSize: " + heapMaxSize);
        */

        /*
         * Execute Dijkstra's algorithm either until a specified destination had been
         * found or until the 'rim' doesn't contain any nodes, i.e. the one-to-all
         * calculation terminated successfully
         *
         * Invariant: Size of queue is limited by number of nodes in graph
         * Variant: Every node of the graph can only be added once to the queue AND
         *          in each step exactly one node is polled from queue -> queue will
         *          be empty in a finite number of steps
         */
        while (!queue.isEmpty()) {
            int shortest = queue.poll(); // Get next node from priority queue
            if (!this.processed[shortest]) {
                this.processed[shortest] = true;

                /*
                 * If currently processed node is the specified destination node, stop
                 * the execution and return the shortest path to destination
                 */
                if (shortest == destination) {
                    computeLastPath(source, destination);
                    return this.distance[shortest];
                }

                /*
                 * Add all nodes which are adjacent to currently processed node to 'rim' if
                 * they're not already in heap and update their distances
                 *
                 * We use a try-catch-block due to performance reasons. We handled the possible
                 * IndexOtOfBounds in previous versions with an if-else-statement which caused heavy
                 * performance drops
                 */
                int lowerBound = this.graph.offsetOf(shortest);
                int upperBound;
                try {
                    upperBound = this.graph.offsetOf(shortest + 1);
                } catch (IndexOutOfBoundsException e) {
                    upperBound = this.graph.getNumberOfEdges();
                }

                /*
                 * Add all adjacent nodes from current node to queue and / or update their distances
                 *
                 * Invariant: i >= 0 AND i < number of edges in graph AND lowerBound and upperBound are finite numbers
                 * Variant: i is incremented in each step of the for-loop -> i will be greater than upperBound
                 *          in a finite number of steps
                 */
                for (int i = lowerBound; i < upperBound; i++) {
                    int next = this.graph.destinationOf(i);
                    if (!this.processed[next]) {
                        /*
                         * add adjacent node if it's not in heap or update distance if adjacent node is already in heap,
                         * restore heap condition since value of next lowered
                         */
                        int dist = this.graph.weightOf(i) + this.distance[shortest];

                        if (this.previous[next] == NO_PREVIOUS || dist < this.distance[next]) {
                            this.distance[next] = dist; // distance to source
                            this.previous[next] = shortest; // previous of node to add is current shortest
                        }
                        queue.add(next);
                    }
                }
            }
        }
        this.previousSource = source; // Remember last source for further executions

        // if no destination had been specified
        if (destination == NOT_AVAILABLE) {
            return ONE_TO_ALL_COMPLETE;
        }

        // if destination is not reachable from given source throw NoPathAvailableException
        if (distance[destination] == DISTANCE_INFINITY)
            throw new NoPathAvailableException("Graph is maybe not connected. " +
                    "No path available from specified source to destination.");

        return distance[destination];
    }

    /**
     * Getter for class variable graph
     *
     * @return Graph one which dijkstra operates
     */
    public Graph getGraph() {
        return this.graph;
    }

    /**
     * Getter for class variable lastPath
     *
     * @return path of last computation
     */
    public List<double[]> getLastPath() {
        return this.lastPath;
    }

    /**
     * Set class attribute path to last path by adding all points between source and destination
     *
     * @param source      source of path
     * @param destination destination of path
     */
    private void computeLastPath(int source, int destination) {
        this.lastPath.clear();
        int current = destination;
        while (current != source) {
            this.lastPath.add(new double[]{this.graph.getLongitudes()[current], this.graph.getLatitudes()[current]});
            current = this.previous[current];
        }
        this.lastPath.add(new double[]{this.graph.getLongitudes()[source], this.graph.getLatitudes()[source]});
    }
}
