package pp19.team10.backend.algorithms;


import pp19.team10.backend.data.Graph;

/**
 * Class for iterative search of nearest neighbour in graph. Only static methods.
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class Iterative {

    // constants for better understanding of the code
    private static final int NO_NEAREST = -1;
    private static final double INFINITY = Double.MAX_VALUE;

    /**
     * Method for searching the nearest node in a given graph two a given coordinate by iterating through all nodes.
     *
     * @param graph     Graph on which the next node should be searched
     * @param latitude  latitude coordinate of given point
     * @param longitude longitude coordinate of given point
     * @return Node of graph with shortest distance to the coordinates
     */
    public static int findNearest(Graph graph, double latitude, double longitude) {

        // init variables
        int nearest = NO_NEAREST;
        double distance = INFINITY;
        int max = graph.getNumberOfNodes();

        // iterating though nodes of graph and search for shortest distance to coordinate
        for (int i = 0; i < max; i++) {
            double nextDistance = distanceEuclid(latitude, graph.getLatitudes()[i], longitude, graph.getLongitudes()[i]);
            // if distance of current node is shorter than current stored distance actualize it
            if (nextDistance < distance) {
                nearest = i;
                distance = nextDistance;
            }
        }
        // return node with shortest distance
        return nearest;
    }

    /**
     * Method for computing a distance between two points A and B by their coordinates. Uses the pythagorean theorem.
     *
     * @param lat1 latitude of point A
     * @param lat2 latitude of point B
     * @param lon1 longitude of point A
     * @param lon2 longitude of point B
     * @return distance between the two points
     */
    private static double distanceEuclid(double lat1, double lat2, double lon1, double lon2) {
        return Math.sqrt(((lat2 - lat1) * (lat2 - lat1)) + ((lon2 - lon1) * (lon2 - lon1)));
    }

}
