package pp19.team10.backend.algorithms;

import pp19.team10.backend.data.KDTree;
import pp19.team10.backend.data.Node;

/**
 * Class for performing a search in a K-d tree
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class SearchKDTree {

    // attributes

    // store these values as class attributes since it's difficult to store them as method parameters du to
    // recursion. Point is that method calls in Java are Call-by-Value and NOT Call-by-Reference and therefore we can't
    // update them during recursion
    private double shortestDistance;
    private Node nearestNode;

    // store references to K-d tree on which the search is performed
    private KDTree kdTree;
    private double[] latitudes;
    private double[] longitudes;

    /**
     * Constructor
     *
     * @param kdTree Reference to kdTree on which search is performed
     */
    public SearchKDTree(KDTree kdTree) {
        this.kdTree = kdTree;
        this.nearestNode = kdTree.getRoot();
        shortestDistance = Double.MAX_VALUE;
        this.latitudes = kdTree.getLatitudes();
        this.longitudes = kdTree.getLongitudes();
    }

    /**
     * Compares the coordinates of a given Node in K-d tree and a coordinate.
     * Depends on mode which dimension is compared
     *
     * @param node      Node in K-d tree
     * @param latitude  Latitude of coordinate
     * @param longitude Longitude of coordinate
     * @param mode      True = Longitudes are compared
     *                  False = Latitudes are compared
     * @return True if compared dimension of node is greater than the given coordinate, false otherwise
     */
    private boolean compare(Node node, double latitude, double longitude, boolean mode) {
        if (mode) return (longitude <= this.longitudes[node.getId()]);
        return (latitude <= this.latitudes[node.getId()]);
    }

    /**
     * Calculate euclidean distance between to given points (by coordinate) (Pythagorean theorem is used)
     *
     * @param lat1 Latitude of first point
     * @param lat2 Latitude of second point
     * @param lon1 Longitude of first point
     * @param lon2 Longitude of second point
     * @return Euclidean distance between the two points
     */
    private double distanceEuclid(double lat1, double lat2, double lon1, double lon2) {
        return Math.sqrt(((lat2 - lat1) * (lat2 - lat1)) + ((lon2 - lon1) * (lon2 - lon1)));
    }

    /**
     * Method for finding the nearest neighbour to a given point (by coordinates).
     *
     * @param latitude  Latitude of the given point
     * @param longitude Longitude of the given point
     * @return ID of nearest neighbour
     */
    public int findNearestNeighbour(double latitude, double longitude) {
        this.nearestNode = kdTree.getRoot();
        this.shortestDistance = Double.MAX_VALUE;
        findNext(latitude, longitude, kdTree.getRoot(), true);
        return this.nearestNode.getId();
    }

    /**
     * Helping method for finding the nearest neighbour to a given point (by coordinates).
     * <p>
     * Inspired by Slides of 'Lecture 13+: Nearest Neighbor search' by Thinh Nguyen from the
     * Oregon State University. The pseudo code is given on slide 15.
     * <p>
     * The search performs a deep search in the referred K-d tree and checks every subtree for a possible
     * nearest neighbour to the given coordinate. To avoid useless searches in subtrees we check if the distance
     * from the given coordinate to the area, represented by the subtree, is shorter than the current shortest
     * distance. If it is, there are potential nearest neighbours in the area (i.e. in the subtree). If not we don't
     * need to have a look at the subtree.
     *
     * @param lat              Latitude of given coordinate
     * @param lon              Longitude of given coordinate
     * @param currentNode      Currently checked Node
     * @param cuttingDimension current dimension (either longitude = true or latitude = false)
     */
    private void findNext(double lat, double lon, Node currentNode, boolean cuttingDimension) {

        // if node doesn't exist break out of method (avoid nullPointerException)
        // could be checked in previous method on the recursion stack but this way avoids huge loss of efficiency
        if (currentNode == null) return;

        // compute distance from current node to given coordinate
        double distance = distanceEuclid(lat, this.latitudes[currentNode.getId()],
                lon, this.longitudes[currentNode.getId()]);

        // if distance equals '0' the current node is exactly on the given coordinate and therefore one nearest point
        if (distance == 0) {
            this.nearestNode = currentNode;
            this.shortestDistance = 0;
            return;
        }

        // if current node's distance is less than the current shortest distance we need to update the nearest node
        // and the shortest distance
        if (distance < this.shortestDistance) {
            this.shortestDistance = distance;
            this.nearestNode = currentNode;
        }

        // if current node is a leaf there are no subtrees to look at
        if (currentNode.isLeaf()) return;

        // If dimension of given coordinate is less or equal than that of the current node search in left subtree
        // is performed first
        if (compare(currentNode, lat, lon, cuttingDimension)) {

            if (compare(currentNode, (lat - this.shortestDistance), (lon - this.shortestDistance), cuttingDimension))
                findNext(lat, lon, currentNode.getLeftChild(), !cuttingDimension);
            if (!compare(currentNode, (lat + this.shortestDistance), (lon + this.shortestDistance), cuttingDimension))
                findNext(lat, lon, currentNode.getRightChild(), !cuttingDimension);
        }

        // If dimension of given coordinate is greater than that of the current node search in right subtree
        // is performed first
        else {
            if (!compare(currentNode, (lat + this.shortestDistance), (lon + this.shortestDistance), cuttingDimension))
                findNext(lat, lon, currentNode.getRightChild(), !cuttingDimension);
            if (compare(currentNode, (lat - this.shortestDistance), (lon - this.shortestDistance), cuttingDimension))
                findNext(lat, lon, currentNode.getLeftChild(), !cuttingDimension);
        }
    }

    /**
     * Getter for longitude array
     *
     * @return longitude array of corresponding graph
     */
    public double[] getLongitudes() {
        return longitudes;
    }

    /**
     * Getter for latitude array
     *
     * @return latitude array of corresponding graph
     */
    public double[] getLatitudes() {
        return latitudes;
    }


}
