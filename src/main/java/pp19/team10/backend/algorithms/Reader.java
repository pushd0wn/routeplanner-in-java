package pp19.team10.backend.algorithms;

import pp19.team10.backend.data.Graph;
import pp19.team10.backend.exceptions.IllegalFileFormatException;

import java.io.*;

/**
 * Class Reader for read in a *.fmi-File containing data for a weighted (non negative), directed graph
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class Reader {

    /**
     * Method for read in a *.fmi-File. File has to be formatted exactly as specified at
     * <a href=https://www.fmi.uni-stuttgart.de/alg/research/stuff/>www.fmi.uni-stuttgart.de/</a>.
     * Different format will cause an error!
     *
     * @param path Path of file to read in. File has to be in *.fmi format
     * @return Instance of class pp19.team10.backend.<b>Graph</b>
     * @throws IllegalFileFormatException If file format is incorrect (for correct file format visit
     *                                    the URL above)
     */
    public Graph readFile(String path) throws IllegalFileFormatException {

        /* New file for Buffered Reader */
        File file = new File(path);

        /* Attributes needed for graph instantiation. All attributes
         * initialized with unusual values for error detection
         */
        int numberOfNodes = -1;
        int numberOfEdges = -1;
        int[] offset = null;
        int[] destination = null;
        int[] weight = null;
        double[] latitudes = null;
        double[] longitudes = null;

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            String line;
            String[] substrings;
            int currentOffset = 0;

            // Skip first 5 lines with meta information
            line = br.readLine();

            // new Exception to check if file is formatted properly
            IllegalFileFormatException ex =
                    new IllegalFileFormatException("File format is incorrect! Please enter path to a .fmi file!");
            if (!line.startsWith("#")) throw ex; // IllegalFileFormatException if file is not formatted properly
            line = br.readLine();
            if (!line.startsWith("#")) throw ex; // IllegalFileFormatException if file is not formatted properly
            line = br.readLine();
            if (!line.startsWith("#")) throw ex; // IllegalFileFormatException if file is not formatted properly
            line = br.readLine();
            if (!line.startsWith("#")) throw ex; // IllegalFileFormatException if file is not formatted properly
            line = br.readLine();
            if (!line.startsWith("")) throw ex; // IllegalFileFormatException if file is not formatted properly

            /* read in number of nodes */
            line = br.readLine();
            numberOfNodes = Integer.parseInt(line);

            /* read in number of edges */
            line = br.readLine();
            numberOfEdges = Integer.parseInt(line);

            /* Initialize arrays for graph */
            offset = new int[numberOfNodes];
            destination = new int[numberOfEdges];
            weight = new int[numberOfEdges];
            latitudes = new double[numberOfNodes];
            longitudes = new double[numberOfNodes];

            /*
             * read in nodes
             */
            for (int i = 0; i < numberOfNodes; i++) {
                line = br.readLine();
                substrings = line.split(" ");
                latitudes[i] = Double.parseDouble(substrings[2]);
                longitudes[i] = Double.parseDouble(substrings[3]);
            }

            /*
             * read in edges
             */
            int source;
            int previousSource = 0;
            for (int i = 0; i < numberOfEdges; i++) {
                line = br.readLine();
                substrings = line.split(" ");
                source = Integer.parseInt(substrings[0]);
                if (previousSource + 1 != source) {
                    while (previousSource != source + 1) {
                        previousSource++;
                        if (previousSource > numberOfNodes - 1) break;
                        offset[previousSource] = currentOffset;
                    }
                }
                currentOffset++;
                if ((source + 1) < numberOfNodes) offset[source + 1] = currentOffset;
                destination[i] = Integer.parseInt(substrings[1]);
                weight[i] = Integer.parseInt(substrings[2]);
                previousSource = source;
            }

            /*
             * Check if file has been read in complete, i.e. that the specified
             * number of nodes and edges are correct
             */
            line = br.readLine();
            if (line != null || offset[numberOfNodes - 1] == 0) throw ex; // || destination[numberOfEdges - 1] == 0 ??

        } catch (IOException e) {
            e.printStackTrace();
        }

        // return graph with specified parameters
        return new Graph(numberOfNodes, numberOfEdges, offset, destination,
                weight, longitudes, latitudes);
    }
}
