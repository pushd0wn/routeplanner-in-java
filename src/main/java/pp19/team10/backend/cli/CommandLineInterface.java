package pp19.team10.backend.cli;

import pp19.team10.backend.algorithms.Dijkstra;
import pp19.team10.backend.algorithms.Reader;
import pp19.team10.backend.data.Graph;
import pp19.team10.backend.data.KDTree;
import pp19.team10.backend.exceptions.IllegalFileFormatException;

import java.util.Scanner;

/**
 * CommandLineInterface class
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
public class CommandLineInterface {

    /**
     * Method to print out Headline of CLI
     */
    private static void printHeader() {

        // Print out header
        System.out.println();
        System.out.println("Command-line Interface of 'Programmierprojekt' 2019");
        System.out.println("Maher Albaba (3338083), Maximilian Kotowsky (3311068)");
        System.out.println();
    }

    /**
     * Method to print out CommandLineInterface Menu of CLI
     *
     * @param sc Scanner to read in user input
     * @return Chosen mode
     */
    private static int printMainMenu(Scanner sc) {
        String line;

        while (true) {
            // Print out main menu
            System.out.println("\n--------------------------------------------------------------------");
            System.out.println("\n- MAIN MENU -");
            System.out.println("\n(1) Benchmark - Load Query and Solution-Files*");
            System.out.println("(2) One-To-All - Compute one-to-all for given source*");
            System.out.println("(3) One-To-One - Compute distance from given source to destination*");
            System.out.println("(4) Nearest Neighbour (iterative)*");
            System.out.println("(5) Nearest Neighbour (kd-Tree)*");
            System.out.println("(6) Average time of finding Nearest Neighbour in K-d tree*");
            System.out.println("(7) Enter a new map to operate on");
            System.out.println("(8) Rebuild K-d tree");
            System.out.println("\n(0) Exit");
            System.out.println("\n* Elapsed time is printed to command line after computation");
            System.out.println("* Enter 'menu' to return to main menu");
            System.out.println("\n--------------------------------------------------------------------");
            System.out.print("\nPlease enter desired mode: ");
            line = sc.next();
            try {
                // Read in and return users
                return Integer.parseInt(line);
            } catch (Exception e) {
                System.out.println("\nInvalid input!");
            }
        }
    }

    /**
     * Instantiates a dijkstra based on a given map
     *
     * @param sc Scanner to read in user input
     * @return Instance of Dijkstra based on the given map file
     * @throws IllegalFileFormatException If file of specified path is invalid
     */
    @Deprecated
    private static Dijkstra initDijkstra(Scanner sc) throws IllegalFileFormatException {

        // Read in path to map file
        String path;
        System.out.print("Please enter path to a map file: ");
        path = sc.next();
        System.out.println();
        System.out.println("Operating on map '" + cropPath(path) + "'");

        // Instantiating dijkstra based on given map file and print out elapsed time
        long startTime = System.currentTimeMillis();
        Dijkstra dijkstra = new Dijkstra(new Reader().readFile(path));
        long finalTime = System.currentTimeMillis();
        System.out.println("Read file in " + (finalTime - startTime) + " ms");

        // return instance of dijkstra
        return dijkstra;
    }

    /**
     * Initiate graph by reading in a file. Asks user for path (absolute or relative) to map file
     *
     * @param sc Scanner for reading in user input
     * @return Graph constructed out of map file
     * @throws IllegalFileFormatException If path is not valid
     */
    private static Graph initGraph(Scanner sc) throws IllegalFileFormatException {
        String path;
        System.out.print("Please enter path to a map file: ");
        path = sc.next();
        System.out.println();
        System.out.println("Operating on map '" + cropPath(path) + "'");

        return new Reader().readFile(path);
    }

    /**
     * Method to extract the name of a map off a given path
     *
     * @param path Path of map file
     * @return Name of the map
     */
    private static String cropPath(String path) {
        return path.substring(path.lastIndexOf('/') + 1, path.lastIndexOf('.'));
    }

    /**
     * CommandLineInterface Method
     */
    public static void mainCLI() {
        try (Scanner sc = new Scanner(System.in)) {

            // print header
            printHeader();

            // instantiate components
            long startTime = System.currentTimeMillis();
            Graph graph = initGraph(sc);
            System.out.println("\nBuilding Graph took " + (System.currentTimeMillis() - startTime) + " ms");

            Dijkstra dijkstra = new Dijkstra(graph);
            Run run = new Run();

            startTime = System.currentTimeMillis();
            //KDTree kdTree = new Run().buildKDTree(graph);
            KDTree kdTree = new KDTree(graph);
            System.out.println("Building K-d tree took " + (System.currentTimeMillis() - startTime) + " ms");

            // switch case for operating modes
            // loops until mode equals 0 which exits program
            int mode = -1;
            while (mode != 0) {
                // print main menu and work in specified mode
                mode = printMainMenu(sc);
                switch (mode) {
                    case 1:
                        System.out.println("\nEntering mode 'Benchmark'");
                        run.benchmark(dijkstra, sc);
                        break;
                    case 2:
                        System.out.println("\nEntering mode 'One-To-All'");
                        run.oneToAll(dijkstra, sc);
                        break;
                    case 3:
                        System.out.println("\nEntering mode 'One-To-One'");
                        run.oneToOne(dijkstra, sc);
                        break;
                    case 4:
                        System.out.println("\nEntering mode 'Nearest Neighbour (iterative)'");
                        run.nextNeighbourIterative(graph, sc);
                        break;
                    case 5:
                        System.out.println("\nEntering mode 'Nearest Neighbour (kd-Tree)'");
                        run.nextNeighbourKDTree(kdTree, sc);
                        break;
                    case 6:
                        System.out.println("\nEntering mode 'Average K-d tree search duration'");
                        run.performDurationTest(kdTree, graph);
                        break;
                    case 7:
                        System.out.println("\nEntering mode 'New Graph'");
                        startTime = System.currentTimeMillis();
                        graph = initGraph(sc);
                        System.out.println("\nBuilding Graph took " + (System.currentTimeMillis() - startTime) + " ms");

                        dijkstra = new Dijkstra(graph);

                        startTime = System.currentTimeMillis();
                        kdTree = new KDTree(graph);
                        System.out.println("Building K-d tree took " + (System.currentTimeMillis() - startTime) + " ms");

                        break;
                    case 8:
                        System.out.println("\nUpdating K-d tree");
                        startTime = System.currentTimeMillis();
                        //kdTree = new Run().buildKDTree(graph);
                        kdTree = new KDTree(graph);
                        System.out.println("Building K-d tree took " + (System.currentTimeMillis() - startTime) + " ms");
                        break;
                    case 0:
                        System.out.println("\nLeaving program...");
                        break;
                    default:
                        System.out.println("\nInvalid Input!");
                }
            }
        } catch (IllegalFileFormatException e) {
            e.printStackTrace();
        }
    }
}
