package pp19.team10.backend.cli;

import pp19.team10.backend.algorithms.Dijkstra;
import pp19.team10.backend.algorithms.Iterative;
import pp19.team10.backend.algorithms.SearchKDTree;
import pp19.team10.backend.data.Graph;
import pp19.team10.backend.data.KDTree;
import pp19.team10.backend.exceptions.GraphException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.Duration;
import java.util.Scanner;

import java.time.Instant;

/**
 * Class for running the different operating modes.
 *
 * @author Maximilian Kotowsky (3311068)
 * @author Maher Albaba (3338083)
 * @version 1.0
 */
class Run {

    /**
     * Class for benchmarks. Asks user via CLI for query and solution file (*.que/*.sol), computes a point-to-point
     * dijkstra for all entries in query file and checks the result with the solution file. After computation the
     * elapsed time is printed out.
     *
     * @param dijkstra Instance of Dijkstra on which the computation should be executed. The instance of dijkstra
     *                 works on a previously specified map file.
     * @param sc       Scanner for CLI
     */
    void benchmark(Dijkstra dijkstra, Scanner sc) {

        try {
            // measuring elapsed time
            long startTime = System.currentTimeMillis();

            // ask for query file
            System.out.print("\nEnter path to 'Query-File': ");
            String que = sc.next();
            System.out.println();

            // end method if input had been 'menu'
            if (que.equals("menu")) return;

            // ask for solution file
            System.out.print("Enter path to 'Solution-File': ");
            String sol = sc.next();
            System.out.println();

            // end method if input had been 'menu'
            if (sol.equals("menu")) return;

            // Buffered Reader for read in query and solution files
            BufferedReader queReader = new BufferedReader(new FileReader(que));
            BufferedReader solReader = new BufferedReader(new FileReader(sol));

            // variables
            String[] queContent;
            String queLine = queReader.readLine();
            String solLine = solReader.readLine();
            int source;
            int previousSource = -1; // to prevent double calculation, initalized with -1 as non used value
            int destination;
            int distanceExpected;
            int counter = 0; // reference to line in which query failed

            // Read in files
            while (queLine != null && solLine != null) {
                queContent = queLine.split(" ");
                source = Integer.parseInt(queContent[0]);
                destination = Integer.parseInt(queContent[1]);
                distanceExpected = Integer.parseInt(solLine);

                // check query and end method if query is incorrect
                if (checkQuery(dijkstra, source, previousSource, destination, distanceExpected) < 0) {
                    System.out.println("\nQuery in line " + counter + " failed!");
                    return;
                }
                counter++;
                previousSource = source;
                queLine = queReader.readLine();
                solLine = solReader.readLine();
            }

            // measuring and print out elapsed time
            long finalTime = System.currentTimeMillis();
            System.out.println("\nMode Benchmark took " + (finalTime - startTime) + " ms in total");

            // close both readers
            queReader.close();
            solReader.close();

        } catch (Exception e) {
            System.out.println("Invalid path!");
        }
    }

    /**
     * Method for operating mode one-to-all. The user is asked for a source and all distances from specified source
     * to all nodes on the graph (of parameter dijkstra) are computed. After computation has finished the user
     * can enter several destinations for which the distance from source is printed out. Method call ends if user
     * enters "menu".
     *
     * @param dijkstra Instance of dijkstra class
     * @param sc       Scanner to read in users input
     */
    void oneToAll(Dijkstra dijkstra, Scanner sc) {

        try {
            // variables for purpose (descriptive names)
            int maxNode = dijkstra.getGraph().getNumberOfNodes() - 1;
            int source;
            int destination;
            int distance;

            // read in source
            System.out.print("\nPlease enter a source: ");
            source = readInNode(sc, maxNode);
            if (source == -1) return; // input had been 'menu' and we return to main menu

            // compute a one-to-all and print out elapsed time
            long startTime = System.currentTimeMillis();
            dijkstra.oneToAll(source);
            long finalTime = System.currentTimeMillis();
            System.out.println("\nComputed all distances from source " + source + " in "
                    + (finalTime - startTime) + " ms");


            while (true) {
                // read in destination
                System.out.print("\nPlease enter a destination: ");
                destination = readInNode(sc, maxNode);
                if (destination == -1) return; // input had been 'menu' and we return to main menu

                // compute distance from source to destination
                distance = dijkstra.algorithm(source, destination);
                System.out.println("\nDistance from source " + source +
                        " to destination " + destination + " amounts to " + distance);
            }
        } catch (Exception e) {
            e.printStackTrace();
            sc.close();
        }
    }

    /**
     * Method for operating mode one-to-one. The user is asked for a source and a destination node
     * and the distance from source to destination is computed. Only one computation per method call.
     *
     * @param dijkstra Instance of dijkstra class
     * @param sc       Scanner to read in users input from CLI
     */
    void oneToOne(Dijkstra dijkstra, Scanner sc) {

        try {
            // variables for purpose (descriptive names)
            int source;
            int destination;
            int distance;
            int maxNode = dijkstra.getGraph().getNumberOfNodes() - 1;

            // read in source
            System.out.print("\nPlease enter a source: ");
            source = readInNode(sc, maxNode);
            if (source == -1) return; // input had been 'menu' and we return to main menu

            // read in destination
            System.out.print("\nPlease enter a destination: ");
            destination = readInNode(sc, maxNode);
            if (destination == -1) return; // input had been 'menu' and we return to main menu

            // compute distance from source to destination and print out elapsed time
            long startTime = System.currentTimeMillis();
            distance = dijkstra.algorithm(source, destination);
            long finalTime = System.currentTimeMillis();
            System.out.println("\nDistance from source " + source + " to destination "
                    + destination + " amounts to " + distance);
            System.out.println("Computation took " + (finalTime - startTime) + " ms");

        } catch (Exception e) {
            e.printStackTrace();
            sc.close();
        }
    }

    /**
     * Method to read in node from CLI
     *
     * @param sc      Scanner
     * @param maxNode Maximum node number of the current graph
     * @return Either number of node or -1 if input was "menu" (i.e. user wants to return to main menu)
     */
    private int readInNode(Scanner sc, int maxNode) {
        // variables for read in user input
        String line;
        line = sc.next();

        // ask user for node number until he enters a valid number or string "menu"
        while (true) {
            // if input is "menu" return -1
            if (line.matches("^menu$")) {
                return -1;
            }

            // if input is a number and on the graph return node
            try {
                int node = Integer.parseInt(line);
                if (node <= maxNode && node >= 0) {
                    return node;
                }
                System.out.println("\nEntered node cannot be found on the graph!");
            } catch (NumberFormatException ignored) {
                System.out.println("\nInput contains invalid characters or is too big!");
            }

            // ask user for valid input
            System.out.println("\nPlease enter valid node or 'menu'.");
            System.out.print("Another try: ");
            line = sc.next();
        }
    }

    /**
     * Checks if a query is correct and returns either 1 if query is correct or -1 is query is incorrect
     *
     * @param dijkstra    instance of Dijkstra which performs the query
     * @param source      Source of query
     * @param destination Destination of query
     * @param distance    Distance from source to destination
     * @return 1 if query is correct or -1 if query is incorrect
     */
    private int checkQuery(Dijkstra dijkstra, int source, int previousSource, int destination, int distance) {

        // variable for result of query, initial value 1 implies query suceeded
        int res = 1;
        try {
            // compute all distances for new source
            if (source != previousSource) {
                System.out.println("Computing all distances from source " + source);
                dijkstra.oneToAll(source); // compute one to all for new source
            }
            // check query and print out elapsed time
            // if query is invalid throw new exception
            long startTime = System.currentTimeMillis();
            if (distance != dijkstra.algorithm(source, destination)) res = -1; // if query failed
            long finalTime = System.currentTimeMillis();
            System.out.println("Query from source " + source
                    + " to destination " + destination + " succeeded "
                    + "in " + (finalTime - startTime) + " ms");
            System.out.println();
        } catch (GraphException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Searches for a node with a shortest distance (might be ambiguous) using a given KDTree
     *
     * @param kdTree kdTree on which the search is performed
     * @param sc     Scanner for reading in user input
     */
    void nextNeighbourKDTree(KDTree kdTree, Scanner sc) {
        // variables for coordinate
        double lat;
        double lon;

        SearchKDTree searchKDTree = new SearchKDTree(kdTree);

        // asking for user input and searching for the next node in KDTree
        while (true) {
            // read in user input
            System.out.print("\nPlease enter Latitude: ");
            lat = readInDouble(sc);
            if (lat == -1) return;
            System.out.print("Please enter Longitude :");
            lon = readInDouble(sc);
            if (lon == -1) return;

            // computation of the next node (with time measurement)
            long startTime = System.nanoTime();
            int node = searchKDTree.findNearestNeighbour(lat, lon);
            long elapsedTime = (System.nanoTime() - startTime) / 1000;

            // print out result
            System.out.println("\nNext node on graph has ID " + node + " with coordinates ("
                    + searchKDTree.getLatitudes()[node] + "," + searchKDTree.getLongitudes()[node] + ")");
            System.out.println("Finding that node took " + elapsedTime + " microseconds");
        }
    }

    /**
     * Searches for a node with a shortest distance (might be ambiguous)
     * by iterating through all nodes of the graph
     *
     * @param graph Graph on which the search is performed
     * @param sc    Scanner for reading in user input
     */
    void nextNeighbourIterative(Graph graph, Scanner sc) {
        // variables for coordinate
        double lat;
        double lon;

        // asking for user input and searching for the next node on the graph
        while (true) {
            // read in user input
            System.out.print("\nPlease enter Latitude: ");
            lat = readInDouble(sc);
            if (lat == -1) return;
            System.out.print("Please enter Longitude :");
            lon = readInDouble(sc);
            if (lon == -1) return;

            // computation of the next node (with time measurement)
            long startTime = System.currentTimeMillis();
            int node = Iterative.findNearest(graph, lat, lon);
            long finalTime = System.currentTimeMillis();

            // print out result
            System.out.println("\nNext node on graph has ID " + node + " with coordinates ("
                    + graph.getLatitudes()[node] + "," + graph.getLongitudes()[node] + ")");
            System.out.println("Finding that node took " + (finalTime - startTime) + " ms");
        }
    }

    /**
     * Method for reading in double values from user
     *
     * @param sc Scanner for reading in user input
     * @return user input
     */
    private double readInDouble(Scanner sc) {
        // variables for read in user input
        String line;
        line = sc.next();

        // ask user for node number until he enters a valid number or string "menu"
        while (true) {
            // if input is "menu" return -1
            if (line.matches("^menu$")) {
                return -1;
            }

            // if input is a number and on the graph return node
            try {
                return Double.parseDouble(line);
            } catch (NumberFormatException ignored) {
                System.out.println("\nInput contains invalid characters!");
            }

            // ask user for valid input
            System.out.println("\nPlease enter valid Lon/Lat or 'menu'.");
            System.out.print("Another try: ");
            line = sc.next();
        }
    }

    /**
     * Method which performs 1000 searches in given K-d tree
     * and calculates the average duration of a search
     *
     * @param kdTree K-d tree in which searches are performed
     * @param graph  Graph of K-d tree (needed for minimum bounding box
     */
    void performDurationTest(KDTree kdTree, Graph graph) {

        // Read in Graph and construct kdTree
        SearchKDTree searchKDTree = new SearchKDTree(kdTree);

        // choose 100 random coordinates within minimum bounding box
        double[][] coordinates = new double[1000][2];

        double[] minBoundingBox = graph.getMinBoundingBox();

        double maxLat = minBoundingBox[0];
        double maxLon = minBoundingBox[1];
        double minLat = minBoundingBox[2];
        double minLon = minBoundingBox[3];

        double lonRange = maxLon - minLon;
        double latRange = maxLat - minLat;

        for (int i = 0; i < 1000; i++) {
            double lat = minLat + (Math.random() * latRange);
            double lon = minLon + (Math.random() * lonRange);
            coordinates[i][0] = lat;
            coordinates[i][1] = lon;
        }

        // variables for average time
        Instant startTime;
        Instant finalTime;
        long time;
        long averageTime = 0;

        // Iterate through random coordinates and search nearest neighbour by iteration and kdTree
        for (int i = 0; i < 1000; i++) {

            double latitude = coordinates[i][0];
            double longitude = coordinates[i][1];


            // startTime = System.nanoTime();
            startTime = Instant.now();
            searchKDTree.findNearestNeighbour(latitude, longitude);
            finalTime = Instant.now();
            time = Duration.between(startTime, finalTime).getNano();
            // finalTime = System.nanoTime();

            averageTime += (time / 1000);
            // averageTime += ((finalTime - startTime) / 1000);
        }

        // calculate average time needed
        averageTime /= 1000;


        // print out average time
        System.out.println("\nFinding the nearest node in K-d tree took " + averageTime + " microseconds in average");

    }
}
