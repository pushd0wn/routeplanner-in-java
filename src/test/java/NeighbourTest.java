import org.junit.Test;

import pp19.team10.backend.algorithms.Iterative;
import pp19.team10.backend.algorithms.Reader;
import pp19.team10.backend.algorithms.SearchKDTree;
import pp19.team10.backend.data.Graph;
import pp19.team10.backend.data.KDTree;
import pp19.team10.backend.exceptions.IllegalFileFormatException;

import static org.junit.Assert.*;

public class NeighbourTest {

    @Test
    public void testIteration() {
        // insert path to map file
        String path = System.getProperty("arg1");
        try {
            // Read in Graph and construct kdTree
            Graph graph = new Reader().readFile(path);
            KDTree kdTree = new KDTree(graph);
            SearchKDTree searchKDTree = new SearchKDTree(kdTree);

            // choose 100 random coordinates within minimum bounding box
            double[][] coordinates = new double[100][2];

            double[] minBoundingBox = graph.calcMinBoundingBox();

            double maxLat = minBoundingBox[0];
            double maxLon = minBoundingBox[1];
            double minLat = minBoundingBox[2];
            double minLon = minBoundingBox[3];

            double lonRange = maxLon - minLon;
            double latRange = maxLat - minLat;

            for (int i = 0; i < 100; i++) {
                double lat = minLat + (Math.random() * latRange);
                double lon = minLon + (Math.random() * lonRange);
                coordinates[i][0] = lat;
                coordinates[i][1] = lon;
            }

            // Iterate through random coordinates and search nearest neighbour by iteration and kdTree
            for (int i = 0; i < 100; i++) {

                double latitude = coordinates[i][0];
                double longitude = coordinates[i][1];

                int iterNode = Iterative.findNearest(graph, latitude, longitude);
                int kdNode = searchKDTree.findNearestNeighbour(latitude, longitude);

                assertEquals(iterNode, kdNode, 0);

            }

        } catch (IllegalFileFormatException e) {
            e.printStackTrace();
        }
    }
}
